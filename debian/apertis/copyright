Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2015, Maximiliano Curia <maxy@debian.org>
License: Permissive

Files: .clang-format
Copyright: 2019, Gernot Gebhard <gebhard@absint.com>
 2019, Christoph Cullmann <cullmann@kde.org>
License: Expat

Files: debian/tests/ciphertest/ciphertest.cpp
Copyright: 2015, Felix Geyer <fgeyer@debian.org>
 2005, 2006, Brad Hards <bradh@frogmouth.net>
 2003, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/aes-cmac/aes-cmac.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: Expat

Files: examples/base64test/base64test.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: Expat

Files: examples/certtest/certtest.cpp
Copyright: 2005, 2006, Brad Hards <bradh@frogmouth.net>
 2003, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/ciphertest/ciphertest.cpp
Copyright: 2005, 2006, Brad Hards <bradh@frogmouth.net>
 2003, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/cms/cmsexample.cpp
Copyright: 2005, 2006, Brad Hards <bradh@frogmouth.net>
 2003, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/cmssigner/*
Copyright: 2003-2007, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/eventhandlerdemo/eventhandlerdemo.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: Expat

Files: examples/hashtest/hashtest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: Expat

Files: examples/hextest/hextest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: Expat

Files: examples/keyloader/keyloader.cpp
Copyright: 2003-2007, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/mactest/mactest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: Expat

Files: examples/md5crypt/md5crypt.cpp
Copyright: 2007, Carlo Todeschini - Metarete s.r.l. <info@metarete.it>
License: Expat

Files: examples/providertest/providertest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: Expat

Files: examples/publickeyexample/publickeyexample.cpp
Copyright: 2005, 2006, Brad Hards <bradh@frogmouth.net>
 2003, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/randomtest/randomtest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: Expat

Files: examples/rsatest/rsatest.cpp
Copyright: 2005, 2006, Brad Hards <bradh@frogmouth.net>
 2003, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/saslclient/saslclient.cpp
Copyright: 2006, Michail Pishchagin <mblsha@gmail.com>
 2003-2008, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/saslserver/saslserver.cpp
Copyright: 2006, Michail Pishchagin <mblsha@gmail.com>
 2003-2008, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/sslservtest/sslservtest.cpp
Copyright: 2005, 2006, Brad Hards <bradh@frogmouth.net>
 2003, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/ssltest/ssltest.cpp
Copyright: 2003-2007, Justin Karneges <justin@affinix.com>
License: Expat

Files: examples/tlssocket/*
Copyright: 2003-2007, Justin Karneges <justin@affinix.com>
License: Expat

Files: include/*
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: include/QtCrypto/qca_basic.h
 include/QtCrypto/qca_core.h
Copyright: 2013-2016, Ivan Romanov <drizt@land.ru>
 2004-2007, Brad Hards <bradh@frogmouth.net>
 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: include/QtCrypto/qca_safetimer.h
 include/QtCrypto/qca_version.h.in
Copyright: 2014, Ivan Romanov <drizt@land.ru>
License: LGPL-2.1+

Files: plugins/qca-botan/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-botan/qca-botan.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: plugins/qca-cyrus-sasl/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-cyrus-sasl/qca-cyrus-sasl.cpp
Copyright: 2006, Michail Pishchagin <mblsha@gmail.com>
 2003-2007, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: plugins/qca-gcrypt/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-gcrypt/hkdf.c
Copyright: 2018, Alexander Volkov <a.volkov@rusbitech.ru>
 2011, Collabora Ltd.
License: LGPL-2.1+

Files: plugins/qca-gcrypt/pkcs5.c
Copyright: 2002, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: plugins/qca-gcrypt/qca-gcrypt.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: plugins/qca-gnupg/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-gnupg/gpgaction.cpp
 plugins/qca-gnupg/gpgaction.h
 plugins/qca-gnupg/gpgop.cpp
 plugins/qca-gnupg/gpgop.h
 plugins/qca-gnupg/gpgop_p.h
 plugins/qca-gnupg/lineconverter.cpp
 plugins/qca-gnupg/lineconverter.h
 plugins/qca-gnupg/mykeystoreentry.cpp
 plugins/qca-gnupg/mykeystoreentry.h
 plugins/qca-gnupg/mykeystorelist.cpp
 plugins/qca-gnupg/mykeystorelist.h
 plugins/qca-gnupg/mymessagecontext.cpp
 plugins/qca-gnupg/mymessagecontext.h
 plugins/qca-gnupg/myopenpgpcontext.cpp
 plugins/qca-gnupg/myopenpgpcontext.h
 plugins/qca-gnupg/mypgpkeycontext.h
 plugins/qca-gnupg/qca-gnupg.cpp
 plugins/qca-gnupg/ringwatch.cpp
 plugins/qca-gnupg/ringwatch.h
Copyright: 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: plugins/qca-gnupg/gpgproc/*
Copyright: 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: plugins/qca-gnupg/utils.cpp
 plugins/qca-gnupg/utils.h
Copyright: 2014, Ivan Romanov <drizt@land.ru>
 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: plugins/qca-logger/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-logger/qca-logger.cpp
Copyright: 2007, Alon Bar-Lev <alon.barlev@gmail.com>
License: LGPL-2.1+

Files: plugins/qca-nss/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-nss/qca-nss.cpp
Copyright: 2006, 2007, Brad Hards <bradh@frogmouth.net>
License: LGPL-2.1+

Files: plugins/qca-ossl/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-ossl/qca-ossl.cpp
Copyright: 2017, Fabian Vogt <fabian@ritter-vogt.de>
 2013-2016, Ivan Romanov <drizt@land.ru>
 2004-2007, Justin Karneges <justin@affinix.com>
 2004-2006, Brad Hards <bradh@frogmouth.net>
License: LGPL-2.1+

Files: plugins/qca-pkcs11/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-pkcs11/qca-pkcs11.cpp
Copyright: 2006, 2007, Alon Bar-Lev <alon.barlev@gmail.com>
 2004, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: plugins/qca-softstore/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-softstore/qca-softstore.cpp
Copyright: 2007, Alon Bar-Lev <alon.barlev@gmail.com>
License: LGPL-2.1+

Files: plugins/qca-test/*
Copyright: 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: plugins/qca-wincrypto/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-wincrypto/qca-wincrypto.cpp
Copyright: 2008, Michael Leupold <lemma@confuego.org>
License: LGPL-2.1+

Files: plugins/qca-wingss/*
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: LGPL-2.1

Files: plugins/qca-wingss/qca-wingss.cpp
Copyright: 2008, Barracuda Networks, Inc.
License: LGPL-2.1+

Files: src/*
Copyright: 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: src/botantools/botan/*
Copyright: 1999-2007, The Botan Project.
License: BSD-2-clause

Files: src/botantools/wrapns.c
Copyright: 2003-2007, Justin Karneges <justin@affinix.com>
License: Expat

Files: src/qca_basic.cpp
 src/qca_cert.cpp
 src/qca_keystore.cpp
 src/qca_plugin.h
 src/qca_publickey.cpp
 src/qca_securelayer.cpp
 src/qca_securemessage.cpp
 src/qca_textfilter.cpp
 src/qca_tools.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: src/qca_core.cpp
Copyright: 2013-2016, Ivan Romanov <drizt@land.ru>
 2004-2007, Brad Hards <bradh@frogmouth.net>
 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: src/qca_default.cpp
Copyright: 2004, 2005, Brad Hards <bradh@frogmouth.net>
 2003-2007, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+ or Zlib

Files: src/qca_safetimer.cpp
Copyright: 2014, Ivan Romanov <drizt@land.ru>
License: LGPL-2.1+

Files: src/support/logger.cpp
Copyright: 2006, 2007, Brad Hards <bradh@frogmouth.net>
License: LGPL-2.1+

Files: tools/mozcerts/main.cpp
Copyright: 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: tools/qcatool/main.cpp
Copyright: 2003-2008, Justin Karneges <justin@affinix.com>
License: LGPL-2.1+

Files: unittest/base64unittest/base64unittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/bigintunittest/bigintunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/certunittest/certunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/cipherunittest/*
Copyright: 2013-2016, Ivan Romanov <drizt@land.ru>
 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/clientplugin/*
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/cms/cms.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/dsaunittest/dsaunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/filewatchunittest/*
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/hashunittest/hashunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/hexunittest/hexunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/kdfunittest/kdfunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/keybundle/keybundle.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/keygenunittest/keygenunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/keylengthunittest/keylengthunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/keystore/keystore.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/logger/loggerunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/macunittest/macunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/metatype/metatype.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/pgpunittest/pgpunittest.cpp
Copyright: 2006, 2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/pipeunittest/pipeunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/pkits/pkits.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/rsaunittest/rsaunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/securearrayunittest/securearrayunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/staticunittest/staticunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/symmetrickeyunittest/symmetrickeyunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/tls/tlsunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: unittest/velox/veloxunittest.cpp
Copyright: 2004-2007, Brad Hards <bradh@frogmouth.net>
License: BSD-2-clause

Files: cmake/modules/FindPkcs11Helper.cmake cmake/modules/FindSasl2.cmake
Copyright: 2006, Laurent Montel <montel@kde.org>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/botantools/botantools.diff unittest/certunittest/certs/OCSPServer.crt unittest/certunittest/certs/RAIZ2007_CERTIFICATE_AND_CRL_SIGNING_SHA256.crt unittest/certunittest/certs/RootCAcert.crt unittest/certunittest/certs/Server.crt unittest/certunittest/certs/Server.p12 unittest/certunittest/certs/Serverrev.p12 unittest/certunittest/certs/User.crt unittest/certunittest/certs/User.p12 unittest/certunittest/certs/Userrev.crt unittest/certunittest/certs/Userrev.p12 unittest/certunittest/certs/ov-root-ca-cert.crt unittest/keybundle/servergood2.p12 unittest/keybundle/user2good.p12 unittest/pkits/certs/BadSignedCACert.crt unittest/pkits/certs/BadnotAfterDateCACRL.crl unittest/pkits/certs/BadnotBeforeDateCACRL.crl unittest/pkits/certs/BadnotBeforeDateCACert.crt unittest/pkits/certs/DSAParametersInheritedCACert.crt unittest/pkits/certs/InvalidCAnotBeforeDateTest1EE.crt unittest/pkits/certs/InvalidDSASignatureTest6EE.crt unittest/pkits/certs/InvalidEESignatureTest3EE.crt unittest/pkits/certs/InvalidEEnotBeforeDateTest2EE.crt unittest/pkits/certs/InvalidMissingCRLTest1EE.crt unittest/pkits/certs/InvalidNameChainingTest1EE.crt unittest/pkits/certs/InvalidRevokedCATest2EE.crt unittest/pkits/certs/Invalidpre2000UTCEEnotAfterDateTest7EE.crt unittest/pkits/certs/NoCRLCACert.crt unittest/pkits/certs/RFC3280MandatoryAttributeTypesCACert.crt unittest/pkits/certs/RevokedsubCACert.crt unittest/pkits/certs/RolloverfromPrintableStringtoUTF8StringCACRL.crl unittest/pkits/certs/RolloverfromPrintableStringtoUTF8StringCACert.crt unittest/pkits/certs/TrustAnchorRootCRL.crl unittest/pkits/certs/UIDCACert.crt unittest/pkits/certs/UTF8StringCaseInsensitiveMatchCACRL.crl unittest/pkits/certs/UTF8StringCaseInsensitiveMatchCACert.crt unittest/pkits/certs/UTF8StringEncodedNamesCACert.crt unittest/pkits/certs/ValidGeneralizedTimenotAfterDateTest8EE.crt unittest/pkits/certs/ValidGeneralizedTimenotBeforeDateTest4EE.crt unittest/pkits/certs/ValidNameChainingCapitalizationTest5EE.crt unittest/pkits/certs/ValidNameUIDsTest6EE.crt unittest/pkits/certs/ValidRFC3280OptionalAttributeTypesTest8EE.crt unittest/pkits/certs/ValidUTF8StringCaseInsensitiveMatchTest11EE.crt unittest/pkits/certs/Validpre2000UTCnotBeforeDateTest3EE.crt
Copyright: 2006-2007, Alon Bar-Lev <alon.barlev@gmail.com>
 2008, Barracuda Networks, Inc
 2004-2007, Brad Hards <bradh@frogmouth.net>
 1991-1999, Free Software Foundation, Inc
 2013-2016, Ivan Romanov <drizt@land.ru>
 2003-2008, Justin Karneges <justin@affinix.com>
 2008, Michael Leupold <lemma@confuego.org>
 2006, Michail Pishchagin <mblsha@gmail.com>
 2017, Gabriel Souza Franco <gabrielfrancosouza@gmail.com>
 2017, Fabian Vogt <fabian@ritter-vogt.de>
License: LGPL-2.1+
